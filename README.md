# Breweries and Beers API

Based on [Opendatasoft](https://data.opendatasoft.com) data, Breweries and Beers API is a node JS secure MVC structured API as part of IT DUT. 

## Installation

This is a repository available on github. Installation is done by following theses few steps:

First, clone the repository and go into it:
```
$ git clone https://gitlab.com/emilienbidet/breweries-beers-api
$ cd breweries-beers-api
```

Then, install all the necessary packages:
```
$ npm install
```

## Deployment

After following installation steps, deploy the API on your local machine by following theses few steps:

Create a ```.env``` file at the root of the project for declaring env variables and fill it with your own configuration like this model:
```
PORT=3000  // port to listen
DATABASE=./api/data/breweries-beers-db.db // path to database
DATABASE_LOGIN=login // database login
DATABASE_PASSWORD=password // database password
JWT_KEY=fgd2fgh7d383eter3tmlglkbuswqrfuer3t70er57revt6e1r6ter67rec // random long private key
```

Run the server to test if all is fine.
```
$ npm start
```

The output should be like this:
```
[Server][Info] Server started on $PORT
[Server][Info] Connected to the database
```

## Usage

### API documentation

See the [API documentation](https://documenter.getpostman.com/view/14956818/Tz5tZwWU) on postman.

### Fill the API

To fill the API with [Opendatasoft](https://data.opendatasoft.com) data do:

Generate a valid token and add it in the ```./utils/config.js``` file.
```js
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJpYXQiOjE2MTYxMDgzMjMsImV4cCI6MTYxNjExMTkyM30.TU1vzXi4czKtMFXi64tR-jWau6BQZH53d3MDT0Fva3E'; // valid token
const interval = 10; // interval between two requests
```

Add **breweries**:
```
$ npm run put-breweries
```

Add **beers**:
```
$ npm run put-beers
```

## Test

Run bash tests by doing this:
```
$ npm test
```
JS tests are not finished to be impleted yet but can run with:
```
$ npm run test-js
```

## Built With

### Framework

Express instead of hapi because we have already worked with express.

* [express](https://github.com/expressjs/express) - Web framework

### Database

First, MongoDB ORM was used to manage the database then changed for sqlite3 with sequelize because the project can't be demo at IUT for a proxy reason.

* [sequelize](https://github.com/sequelize/sequelize) - ORM tool
* [sqlite3](https://github.com/mapbox/node-sqlite3) - SQLite3 bindings

### Data

Thanks to Opendatasoft data, the API can have real data.

* [Opendatasoft](https://data.opendatasoft.com) - Open data network

### Middlewares and other libraries

Bcrypt used to secure password in the database, jsonwebtoken to generate login token and finally morgan to see live requests but only for development.

* [bcrypt](https://github.com/kelektiv/node.bcrypt.js) - Hash password library
* [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - JSON Web Tokens library
* [morgan](https://github.com/expressjs/morgan) - HTTP request logger middleware

### Documentation generator and test software

* [Postman](https://www.postman.com/) - Collaboration platform for API development

## Roadmap

- [X] REST API with /breweries and /beers routes
- [X] MVC structured
- [X] /signup and /login routes and /users route
- [X] Protect /breweries and /beers routes
- [X] Generate API documentation
- [X] Add script to fill the database from opendata
- [ ] Add an id to link a beer to a brewery
- [ ] /breweries GET : add an argument to select the desired page
- [ ] /beers GET : add an argument to select the desired page
- [ ] Implement websockets

## Authors

* Emilien BIDET - main developer
* Maxime POTET - developer

## Special thanks

* JF. Berdjugin - IT teacher from IUT Nantes
* Opendatasoft - Data provider