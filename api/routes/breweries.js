import express from 'express';
import * as BreweriesController from '../controllers/breweries';

const router = express.Router();

// Handle incoming GET requests to /breweries
router.get('/', BreweriesController.breweries_get_all);

// Handle incoming POST requests to /breweries
router.post('/', BreweriesController.breweries_post_brewery);

// Handle incoming GET requests to /breweries/:breweryId
router.get('/:breweryId', BreweriesController.breweries_get_brewery);

// Handle incoming PATCH requests to /breweries/:breweryId
router.patch('/:breweryId', BreweriesController.breweries_patch_brewery);

// Handle incoming DELETE requests to /breweries/:breweryId
router.delete('/:breweryId', BreweriesController.breweries_delete_brewery);

export default router;