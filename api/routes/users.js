import express from 'express';
import * as UsersController from '../controllers/users';

const router = express.Router();

// Handle incoming POST requests to /users/signup
router.post('/signup', UsersController.users_post_signup);

// Handle incoming POST requests to /users/login
router.post('/login', UsersController.users_post_login);

// Handle incoming DELETE requests to /users/:userId
router.delete('/:userId', UsersController.users_delete_user);

export default router;