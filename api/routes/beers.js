import express from 'express';
import * as BeersController from '../controllers/beers';

const router = express.Router();

// Handle incoming GET requests to /beers
router.get('/', BeersController.beers_get_all);

// Handle incoming POST requests to /beers
router.post('/', BeersController.beers_post_beer);

// Handle incoming GET requests to /beers/:beerId
router.get('/:beerId', BeersController.beers_get_beer);

// Handle incoming PATCH requests to /beers/:beerId
router.patch('/:beerId', BeersController.beers_patch_beer);

// Handle incoming DELETE requests to /beers/:beerId
router.delete('/:beerId', BeersController.beers_delete_beer);

export default router;