import { models } from '../../app';

const Beer = models.Beer;

function beers_get_all(req, res, next) {
    Beer.findAll({ limit: 50 })
        .then(beers => {
            res.status(200).json(beers);
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function beers_post_beer(req, res, next) {
    if (req.body.name == undefined) {
        return res.status(422).json({
            message: 'Name missing'
        });
    }
    if (req.body.alcohol == undefined) {
        return res.status(422).json({
            message: 'Alcohol missing'
        });
    }
    Beer.create({
        name: req.body.name,
        alcohol: req.body.alcohol,
        description: req.body.description,
        country: req.body.country,
        city: req.body.city
    })
        .then(beer => {
            res.status(201).json(beer);
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function beers_get_beer(req, res, next) {
    Beer.findOne({
        where: {
            id: req.params.beerId
        }
    })
        .then(beer => {
            if (beer) {
                return res.status(200).json(beer);
            }
            res.status(404).json({
                message: 'No beer found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function beers_patch_beer(req, res, next) {
    Beer.findOne({
        where: {
            id: req.params.beerId
        }
    })
        .then(beer => {
            if (beer) {
                for (const ops of req.body) {
                    beer[ops.propName] = ops.value;
                }
                beer.save();
                return res.status(200).json({
                    message: 'Beer patched'
                });
            }
            res.status(404).json({
                message: 'No beer found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function beers_delete_beer(req, res, next) {
    Beer.findOne({
        where: {
            id: req.params.beerId
        }
    })
        .then(beer => {
            if (beer) {
                beer.destroy();
                return res.status(200).json({
                    message: 'Beer deleted'
                });
            }
            res.status(404).json({
                message: 'No beer found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

export { beers_get_all, beers_post_beer, beers_get_beer, beers_patch_beer, beers_delete_beer };