import { models } from '../../app'

const Brewery = models.Brewery;

function breweries_get_all(req, res, next) {
    Brewery.findAll({ limit: 50 })
        .then(breweries => {
            res.status(200).json(breweries);
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function breweries_post_brewery(req, res, next) {
    if (req.body.name == undefined) {
        return res.status(422).json({
            message: 'Name missing'
        });
    }
    Brewery.create({
        name: req.body.name,
        country: req.body.country,
        city: req.body.city
    })
        .then(brewery => {
            res.status(201).json(brewery);
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function breweries_get_brewery(req, res, next) {
    Brewery.findOne({
        where: {
            id: req.params.breweryId
        }
    })
        .then(brewery => {
            if (brewery) {
                return res.status(200).json(brewery);
            }
            res.status(404).json({
                message: 'No brewery found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: error
            });
        });
};

function breweries_patch_brewery(req, res, next) {
    Brewery.findOne({
        where: {
            id: req.params.breweryId
        }
    })
        .then(brewery => {
            if (brewery) {
                for (const ops of req.body) {
                    brewery[ops.propName] = ops.value;
                }
                brewery.save();
                return res.status(200).json({
                    message: 'Brewery patched'
                });
            }
            res.status(404).json({
                message: 'No brewery found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: error
            });
        });
};

function breweries_delete_brewery(req, res, next) {
    Brewery.findOne({
        where: {
            id: req.params.breweryId
        }
    })
        .then(brewery => {
            if (brewery) {
                brewery.destroy();
                return res.status(200).json({
                    message: 'Brewery deleted'
                });
            }
            res.status(404).json({
                message: 'No brewery found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: error
            });
        });
};

export { breweries_get_all, breweries_post_brewery, breweries_get_brewery, breweries_patch_brewery, breweries_delete_brewery };