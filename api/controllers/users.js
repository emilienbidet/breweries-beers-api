import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { models } from '../../app';

const User = models.User;
const emailFormat = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

function users_post_signup(req, res, next) {
    if (req.body.email == undefined) {
        return res.status(422).json({
            message: 'Email missing'
        });
    }
    if (req.body.password == undefined) {
        return res.status(422).json({
            message: 'Password missing'
        });
    }
    if (!emailFormat.test(req.body.email)) {
        return res.status(422).json({
            message: 'Email is not well formated'
        });
    }
    User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(user => {
            if (user) {
                return res.status(409).json({
                    message: 'Email already exists'
                });
            }
            bcrypt.hash(req.body.password, 10, (error, hash) => {
                if (error) {
                    return res.status(500).json({
                        error: 'Server error'
                    });
                }
                User.create({
                    email: req.body.email,
                    password: hash
                })
                    .then(user => {
                        res.status(201).json({
                            message: 'User created'
                        })
                    })
                    .catch(error => {
                        res.status(500).json({
                            error: 'Server error'
                        });
                    });
            });
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function users_post_login(req, res, next) {
    if (req.body.email == undefined) {
        return res.status(422).json({
            message: 'Email missing'
        });
    }
    if (req.body.password == undefined) {
        return res.status(422).json({
            message: 'Password missing'
        });
    }
    if (!emailFormat.test(req.body.email)) {
        return res.status(422).json({
            message: 'Email is not well formated'
        });
    }
    User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(user => {
            if (!user) {
                return res.status(401).json({
                    message: 'Auth failed'
                });
            }
            bcrypt.compare(req.body.password, user.password, (error, same) => {
                if (error) {
                    return res.status(401).json({
                        message: 'Auth failed'
                    });
                }
                if (same) {
                    const token = jwt.sign({
                        id: user._id,
                        email: user.email,
                    },
                        process.env.JWT_KEY,
                        {
                            expiresIn: "1h"
                        });
                    return res.status(200).json({
                        message: "Auth sucessful",
                        token: token
                    });
                } else {
                    return res.status(401).json({
                        message: 'Auth failed'
                    });
                }
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};

function users_delete_user(req, res, next) {
    User.findOne({
        where: {
            id: req.params.userId
        }
    })
        .then(user => {
            if (user) {
                user.destroy();
                return res.status(200).json({
                    message: 'User deleted'
                });
            }
            res.status(404).json({
                message: 'No user found with the id provided'
            });
        })
        .catch(error => {
            res.status(500).json({
                error: 'Server error'
            });
        });
};
export { users_post_signup, users_post_login, users_delete_user };