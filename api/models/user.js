const user = (sequelize, DataTypes) => {
    // Define the model
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },
        email: { 
            type: DataTypes.STRING , 
            allowNull: false, 
            unique: true,
            isEmail: true,
        },
        password: { type: DataTypes.STRING, allowNull: false },
    });
    return User;
};

export default user;