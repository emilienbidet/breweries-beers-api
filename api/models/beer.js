const beer = (sequelize, DataTypes) => {
    // Define the model
    const Beer = sequelize.define('Beer', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: { type: DataTypes.STRING, allowNull: false },
        alcohol: { type: DataTypes.INTEGER, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: true },
        country: { type: DataTypes.STRING, allowNull: true },
        city: { type: DataTypes.STRING, allowNull: true }
    });
    return Beer;
};

export default beer;