const brewery = (sequelize, DataTypes) => {
    // Define the model
    const Brewery = sequelize.define('Brewery', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: { type: DataTypes.STRING, allowNull: false },
        country: { type: DataTypes.STRING, allowNull: true },
        city: { type: DataTypes.STRING, allowNull: true }
    });
    return Brewery;
};

export default brewery;