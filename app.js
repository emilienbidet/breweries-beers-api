import express from 'express';
import morgan from 'morgan';

import models, {sequelize} from './db-init';
import checkAuth from './api/middleware/check-auth';

import breweriesRoutes from './api/routes/breweries';
import beersRoutes from './api/routes/beers';
import usersRoutes from './api/routes/users';

const app = express();

// Initiate the database
// add { force: true } into sync function to clear the database
sequelize.sync({ force: true }).then(async () => {
    console.log("[Server][Info] Connected to the database");
});

// Initiate the middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Handling the CORS error
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers', 
        'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === "OPTIONS") {
        res.header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE');
        return res.status(200).json({});
    }
    next();
});

// Handling routes
app.use('/breweries', checkAuth, breweriesRoutes);
app.use('/beers', checkAuth, beersRoutes);
app.use('/users', usersRoutes);

// Handling 404 error
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

// Returning error message
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json( {
        error: {
            message: error.message
        }
    })
});

export { app, models };