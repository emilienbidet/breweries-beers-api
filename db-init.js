import Sequelize from 'sequelize';
import beer from './api/models/beer';
import brewery from './api/models/brewery';
import user from './api/models/user';

// Create the database
const sequelize = new Sequelize(
    process.env.DATABASE,
    process.env.DATABASE_USER,
    process.env.DATABASE_PASSWORD,
    {
        dialect: 'sqlite',
        logging: false,
        storage: process.env.DATABASE
    },
);

// Import models
beer(sequelize, Sequelize.DataTypes);
brewery(sequelize, Sequelize.DataTypes);
user(sequelize, Sequelize.DataTypes);

const models = sequelize.models;

export { sequelize };
export default models;