import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server';

chai.use(chaiHttp);

chai.should();
beforeEach(done => setTimeout(done, 500));
describe("Breweries", () => {
    describe("GET /breweries", () => {
        // Test to get all students record
        it("should get all breweries record limited by 50", (done) => {
            chai.request(app)
                .get('/breweries')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
        // Test to get single brewery record
        it("should get a single brewery record", (done) => {
            const id = 1;
            chai.request(app)
                .get(`/breweries/${id}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

        // Test to get single brewery record
        it("should not get a single brewery record", (done) => {
            const id = 5;
            chai.request(app)
                .get(`/breweries/${id}`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});