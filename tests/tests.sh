#!/bin/bash
# ----------------------] Users [----------------------

# POST /users/signup
echo 'POST /users/signup'
curl --location --request POST 'http://localhost:3000/users/signup' \
    --header "Content-Type: application/json" \
    --data '{ "email": "test@test.com", "password": "password" }'
echo

# POST /users/login
echo 'POST /users/login'
curl --location --request POST 'http://localhost:3000/users/login' \
    --header "Content-Type: application/json" \
    --data '{ "email": "test@test.com", "password": "password" }'
# Store the token
token=`curl --location --request POST 'http://localhost:3000/users/login' \
    --header "Content-Type: application/json" \
    --data '{ "email": "test@test.com", "password": "password" }' --silent | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/^token/ {print $2}' | sed 's/"//g' `
echo

# DELETE /users/:userId
echo 'POST /users/:userId'
curl --location --request DELETE 'http://localhost:3000/users/1'
echo

# ----------------------] Breweries [----------------------
echo $token
# GET /breweries
echo 'GET /breweries'
curl --location -H "Authorization: Bearer $token" --request GET 'http://localhost:3000/breweries/'
echo

# POST /breweries
echo 'POST /breweries'
curl --location -H "Authorization: Bearer $token" --request POST 'http://localhost:3000/breweries/' \
    --header "Content-Type: application/json" \
    --data '{
        "name": "Brewery example",
        "country": "France",
        "city": "Nantes"
    }'
echo

# GET /breweries/:breweryId
echo 'GET /breweries/:breweryId'
curl --location -H "Authorization: Bearer $token" --request GET 'http://localhost:3000/breweries/1'
echo

# PATCH /breweries/:breweryId
echo 'PATCH /breweries/:breweryId'
curl --location -H "Authorization: Bearer $token" --request PATCH 'http://localhost:3000/breweries/1' \
    --header "Content-Type: application/json" \
    --data '[
        { "propName": "name", "value": "Brewery example" },
        { "propName": "country", "value": "France" },
        { "propName": "city", "value": "Nantes" }
    ]'
echo

# DELETE /breweries/:breweryId
echo 'DELETE /breweries/:breweryId'
curl --location -H "Authorization: Bearer $token" --request DELETE 'http://localhost:3000/breweries/1'
echo

# ----------------------] Beers [----------------------

# GET /beers
echo 'GET /beers'
curl --location -H "Authorization: Bearer $token" --request GET 'http://localhost:3000/beers/'
echo

# POST /beers
echo 'POST /beers'
curl --location -H "Authorization: Bearer $token" --request POST 'http://localhost:3000/beers/' \
    --header "Content-Type: application/json" \
    --data '{
        "name": "Beer example",
        "alcohol": 5,
        "country": "France",
        "city": "Nantes"
    }'
echo

# GET /beers/:beerId
echo 'GET /beers/:beerId'
curl --location -H "Authorization: Bearer $token" --request GET 'http://localhost:3000/beers/1'
echo

# PATCH /beers/:beerId
echo 'PATCH /beers/:beerId'
curl --location -H "Authorization: Bearer $token" --request PATCH 'http://localhost:3000/beers/1' \
    --header "Content-Type: application/json" \
    --data '[
        { "propName": "name", "value": "Beer example" },
        { "propName": "alcohol", "value": 5 },
        { "propName": "country", "value": "France" },
        { "propName": "city", "value": "Nantes" }
    ]'
echo

# DELETE /beers/:beerId
echo 'DELETE /beers/:beerId'
curl --location -H "Authorization: Bearer $token" --request DELETE 'http://localhost:3000/beers/1'
echo