import beers from './data/open-beer-database.json';
import * as utils from './config';

utils.options.path = '/beers';

const beersReady = beers.map(beer => beer.fields)
    .map(beer => {
        const res = {}
        if (beer.name) {
            res.name = utils.cleanString(beer.name);
        }
        res.alcohol = beer.abv | 0;
        if (beer.descript) {
            res.description = utils.cleanString(beer.descript);
        }
        if (beer.country) {
            res.country = utils.cleanString(beer.country);
        }
        if (beer.city) {
            res.city = utils.cleanString(beer.city);
        }
        return res;
    })
    .filter(beer => beer.name != null);

utils.requestsAndWait(utils.options, beersReady);