import breweries from './data/open-beer-database-breweries.json';
import * as utils from './config';

utils.options.path = '/breweries';

const breweriesReady = breweries.map(brewery => brewery.fields)
    .map(brewery => {
        const res = {};
        if (brewery.name_breweries) {
            res.name = utils.cleanString(brewery.name_breweries);
        }
        if (brewery.country) {
            res.country = utils.cleanString(brewery.country);
        }
        if (brewery.city) {
            res.city = utils.cleanString(brewery.city);
        }
        return res;
    })
    .filter(brewery => brewery.name != null);

utils.requestsAndWait(utils.options, breweriesReady);