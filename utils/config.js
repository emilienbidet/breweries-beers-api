import http from 'http';

// Valid token
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJpYXQiOjE2MTYxMDgzMjMsImV4cCI6MTYxNjExMTkyM30.TU1vzXi4czKtMFXi64tR-jWau6BQZH53d3MDT0Fva3E';
// Interval between two requests in ms.
const interval = 10;

const options = {
    hostname: 'localhost',
    port: 3000,
    method: 'POST',
    headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
    }
};

// Function utils
function cleanString(input) {
    let output = "";
    for (var i = 0; i < input.length; i++) {
        if (input.charCodeAt(i) <= 127) {
            output += input.charAt(i);
        }
    }
    return output;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function request(options, data) {
    const datatify = JSON.stringify(data);
    options.headers['Content-Length'] = datatify.length;

    const req = http.request(options);

    req.write(datatify);
    req.end();
}

async function requestsAndWait(options, datas) {
    console.log("[Utils][Info] Start putting data");
    for (let index = 0; index < datas.length; index++) {
        request(options, datas[index]);
        await sleep(interval);
    }
    console.log("[Utils][Info] Putting data done");
}

export { options, cleanString, requestsAndWait }